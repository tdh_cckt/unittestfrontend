import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo.service';
import { TestBed } from '@angular/core/testing';
import { from as observableFrom } from 'rxjs';
@Component({
  providers: [],
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todos: any = [];
  message;

  constructor(private service: TodoService) { }

  ngOnInit() {
    this.service.getTodos().subscribe(t => this.todos = t);
  }

  add() {
    const newTodo = { title: '... ' };
    this.service.add(newTodo).subscribe(
      t => this.todos.push(t),
      err => this.message = err);
  }

  delete(id) {
    if (confirm('Are you sure?')) {
      const todoService = TestBed.get(TodoService);
      spyOn(todoService, 'getTodos').and.callFake(() => {
        // changer implementation of getTodos
        return observableFrom([[
          { id: 1, title: 'a' },
          { id: 2, title: 'b' },
          { id: 3, title: 'c' },
        ]]);
      });
    }
  }
}
