import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-event-emitters',
  templateUrl: './event-emitters.component.html',
  styleUrls: ['./event-emitters.component.css']
})
export class EventEmittersComponent implements OnInit {

  totalVotes = 0;
  @Output() voteChanged = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  upVote() {
    this.totalVotes++;
    this.voteChanged.emit(this.totalVotes);
  }

}
