import {VoteComponent} from './vote.component';

describe('VoteComponent', () => {
    let component: VoteComponent;
    beforeEach(() => {
        component = new VoteComponent();
    });
    // sau mooi cai # afterall
    afterEach(() => {
    });

    // const component = new VoteComponent();

    it('should increment totalVote when upvote', () => {

        component.upVote();

        expect(component.totalVote).toBe(1);
    });
    it('should decrement totalVote when downvote', () => {


        component.downVote();

        expect(component.totalVote).toBe(-1);
    });
});
