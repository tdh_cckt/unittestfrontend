/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { TodoFormComponent } from './todo-form.component';

describe('TodoFormComponent', () => {
  let component: TodoFormComponent;

  beforeEach((() => {
    component = new TodoFormComponent(new FormBuilder());
  }));

  // beforeEach(() => {

  // });

  it('should create a form with 2 controls', () => {
    expect(component.form.contains('name')).toBe(true);
    expect(component.form.contains('email')).toBe(true);
  });
  it('should make the name control required', () => {
    const control = component.form.get('name');

    control.setValue('');
    expect(control.valid).toBeFalsy();
  });
  it('should make the emal control maxleng smaller than 10', () => {
    const control = component.form.get('email');

    control.setValue('12345678910');
    expect(control.valid).toBeFalsy();
  });
});
