import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {
  form: FormGroup;
  constructor(fb: FormBuilder) {
    // 2 test: how many form control ? 2
    // form control has required ? yes
    this.form = fb.group({
      name: ['', Validators.required],
      email: ['', Validators.maxLength(10)],
    });
   }

  ngOnInit() {
  }

}
