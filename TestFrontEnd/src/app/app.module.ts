import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { EventEmittersComponent } from './event-emitters/event-emitters.component';
import { TodoComponent } from './todo/todo.component';
import { HttpClient } from '@angular/common/http';
import { VoterComponent } from './IntegrationTest/1-voter/voter.component';

@NgModule({
   declarations: [
      AppComponent,
      TodoFormComponent,
      EventEmittersComponent,
      TodoComponent,
      VoterComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClient
   ],
   providers: [
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
