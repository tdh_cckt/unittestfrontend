import { async } from '@angular/core/testing';

import { TodoComponent } from './todo.component';
import { TodoServiceService } from '../_services/todoService.service';
import { Observable } from 'rxjs';
import { from as observableFrom } from 'rxjs';
import { empty as observableEmpty } from 'rxjs';
import { throwError as observableThrow } from 'rxjs';
describe('TodoComponent', () => {
  let component: TodoComponent;
  let todoService: TodoServiceService;

  beforeEach(() => {
    todoService = new TodoServiceService(null);
    component = new TodoComponent(todoService);
  });


  it('should set todos properties with the items returned from service', () => {
    spyOn(todoService, 'getTodos').and.callFake(() => {
      // changer implementation of getTodos
      return observableFrom([[
        { id: 1, title: 'a' },
        { id: 2, title: 'b' },
        { id: 3, title: 'c' },
      ]]);
    });
    component.ngOnInit();

    expect(component.todos.length).toBe(3);
  });
  // first test
  it('should call the server to save the changes when a new', () => {
    const spy = spyOn(todoService, 'add').and.callFake(t => {
      return observableEmpty();
    });

    component.add();

    expect(spy).toHaveBeenCalled();
  });
  it('should addthe new todo returned from server', () => {
    const todo = { id: 1 };
    const spy = spyOn(todoService, 'add').and.callFake(t => {
      return observableFrom([todo]);
    });

    component.add();

    expect(component.todos.indexOf(todo)).toBeGreaterThan(-1);
  });
  it('should set message property if server return error', () => {
    const error = 'error from serve';
    const spy = spyOn(todoService, 'add').and.returnValue(observableThrow(error));

    component.add();

    expect(component.message).toBe(error);

  });

  it('sould call server to delete a todo item if the user confimr', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    const spy = spyOn(todoService, 'delete').and.returnValue(observableEmpty());

    component.delete(1);

    expect(spy).toHaveBeenCalledWith(1);
  });

  it('sould NOT call server to delete a todo item if the user Cancle', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    const spy = spyOn(todoService, 'delete').and.returnValue(observableEmpty());

    component.delete(1);

    expect(spy).not.toHaveBeenCalledWith(1);
  });
});
