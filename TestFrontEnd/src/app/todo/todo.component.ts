import { Component, OnInit } from '@angular/core';
import { TodoServiceService } from '../_services/todoService.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  constructor(private todoService: TodoServiceService) { }

  todos: any = [];
  message;
  ngOnInit() {
    this.todoService.getTodos().subscribe(t => this.todos = t);
  }

  add() {
    const newTodoo = {title: '...'};
    this.todoService.add(newTodoo).subscribe(
      t => this.todos.push(t),
      err => this.message = err
    );
  }

  delete(id) {
    if (confirm('Are you sure to delete this?')) {
      this.todoService.delete(id).subscribe();
    }
  }

}
