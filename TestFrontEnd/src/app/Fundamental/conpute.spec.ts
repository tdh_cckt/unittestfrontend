import { compute } from './compute';
describe('compute', () => {
    it('return 0 if input smaller than 0', () => {
        const result = compute(-1);
        expect(result).toBe(0);
    });
    it('return 1 if input greater than 0', () => {
        const result = compute(6);
        expect(result).toBe(1);
    });
});
describe('compute', () => {
    it('return 0 if input smaller than 0', () => {
        const result = compute(-1);
        expect(result).toBe(0);
    });
    it('return 1 if input greater than 0', () => {
        const result = compute(6);
        expect(result).toBe(1);
    });
});
