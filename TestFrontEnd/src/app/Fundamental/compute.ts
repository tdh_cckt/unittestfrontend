// tslint:disable-next-line:variable-name
export function compute(number) {
    if (number < 0) {
        return 0;
    }
    return 1;
}

// tslint:disable-next-line:variable-name
export function compute2(number) {
    if (number < 0) {
        return 'smaller than 0';
    }
    return 'greater than 0';
}
