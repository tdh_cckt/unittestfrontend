import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TodoServiceService {

constructor(private http: HttpClient) { }

getTodos() {
  return this.http.get('link');
}

add(todo) {
  return this.http.post('link', todo);
}

delete(id) {
    return this.http.delete('link');
}
}
