﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StubAndMock
{
    public interface IWebService
    {
        void LogError(string message);
    }
}
