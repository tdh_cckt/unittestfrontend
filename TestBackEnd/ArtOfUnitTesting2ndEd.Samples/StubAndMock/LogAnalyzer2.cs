﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StubAndMock
{
    public class LogAnalyzer2
    {
        public LogAnalyzer2(IEmailService email, IWebService service)
        {
            Email = email;
            Service = service;
        }

        public IEmailService Email { get; set; }
        public IWebService Service { get; set; }
        public void Analyze(string fileName)
        {
            if (fileName.Length < 8)
            {
                try
                {
                    Service.LogError("Filename too short:" + fileName);
                }
                catch (Exception e)
                {
                    Email.SendEmail("someone@somewhere.com",
                    "can't log", e.Message);
                }
            }
        }
    }
}
