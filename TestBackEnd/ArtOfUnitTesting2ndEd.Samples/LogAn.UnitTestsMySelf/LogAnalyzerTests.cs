﻿using NUnit.Framework;
using System;
using FluentAssertions;

namespace LogAn.UnitTestsMySelf
{
    [TestFixture]
    public class LogAnalyzerTests
    {
        private LogAnalyzer _logAnalyzer;

        [SetUp]
        public void InitInstance()
        {
            _logAnalyzer = new LogAnalyzer();
        }

        [TearDown]
        public void EndTest()
        {
            Console.WriteLine("the ends");
        }

        [Test]
        public void IsValidLogFileName_BadExtension_ReturnsFalse()
        {
            LogAnalyzer logAnalyzer = new LogAnalyzer();
            var result = logAnalyzer.IsValidLogFileName("abc.foo");
            Assert.False(result);
        }

        [Test]
        [Category("Test Fast")]
        public void IsValidLogFileName_GoodExtensionLowercase_ReturnsTrue()
        {
            LogAnalyzer logAnalyzer = new LogAnalyzer();
            var result = logAnalyzer.IsValidLogFileName("abc.slf");
            Assert.True(result);
        }

       /// <summary>
       ///  refactor giup test de doc va bao tri hon, vs nhieu truong hop test case vs cac tham so khac nhau
       /// </summary>
       /// <param name="fileName"> tham so truyen vao se dc binding tu paramenter cuar testCase</param>
        [TestCase("abc.slf")]
        [TestCase("abc.SLF")]
        public void IsValidLogFileName_ValidExtensions_ReturnsTrue(string fileName)
        {
            LogAnalyzer logAnalyzer = new LogAnalyzer();
            var result = logAnalyzer.IsValidLogFileName(fileName);
            Assert.True(result);
        }

        /// <summary>
        /// testcase vs nhieu tham so, ca expected return khac nhau cua test case
        /// </summary>
        /// <param name="fileName"> ten file</param>
        /// <param name="expected"> dung cho ca true va false tuy theo return minh expected</param>
        [TestCase("abc.foo", false)]
        [TestCase("abc.slf", true)]
        [TestCase("abc.SLF", true)]
        public void IsValidLogFileName_VariousExtensions_CheckThem(string fileName, bool expected)
        {
            var result = _logAnalyzer.IsValidLogFileName(fileName);
            Assert.AreEqual(expected, result);
        }
      

        [Test]
        public void IsValidLogFileName_EmptyFileName_ThrowException()
        {
            var exception = Assert.Catch<Exception>(() => _logAnalyzer.IsValidLogFileName(string.Empty));
            StringAssert.Contains("filename has to be provi", exception.Message);

        }

        [Test]
        public void IsValidLogFileName_WhenCalled_ChangesWasLastFileNameValid()
        {
            LogAnalyzer logAnalyzer = new LogAnalyzer();
            var result = logAnalyzer.IsValidLogFileName("badName.foo");
            Assert.False(result);
        }

        [TestCase("badname.foo", false)]
        [TestCase("abc.SLF", true)]
        public void IsValidLogFileName_WhenCalled_ChangesWasLastFileNameValid(string fileName, bool expected)
        {
            LogAnalyzer logAnalyzer = new LogAnalyzer();
            logAnalyzer.IsValidLogFileName(fileName);
            Assert.AreEqual(expected, logAnalyzer.WasLastFileNameValid);
        }
    }
}
