﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace StubAndMock.UnitTests
{
    [TestFixture]
    public class LogAnalyzer2Tests
    {
        [Test]
        public void Analyze_WebServiceThrows_SendsEmail()
        {
            FakeWebService2 stubService = new FakeWebService2();
            stubService.ToThrow = new Exception("fake exception");
            FakeEmailService mockEmail = new FakeEmailService();
            LogAnalyzer2 log = new LogAnalyzer2(mockEmail, stubService);
            string tooShortFileName = "abc.ext";
            log.Analyze(tooShortFileName);
            StringAssert.Contains("someone@somewhere.com", mockEmail.To);
            StringAssert.Contains("can't log", mockEmail.Subject);
            StringAssert.Contains("fake exception", mockEmail.Body);
        }
    }
}
