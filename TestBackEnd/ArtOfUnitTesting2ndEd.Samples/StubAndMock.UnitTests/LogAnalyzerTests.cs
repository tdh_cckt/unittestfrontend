﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace StubAndMock.UnitTests
{
    [TestFixture]
    public class LogAnalyzerTests
    {
        [Test]
        public void Analyze_TooShortFileName_CallsWebService()
        {
            FakeWebSerivce mockService = new FakeWebSerivce();
            LogAnalyzer log = new LogAnalyzer(mockService);
            string tooShortFileName = "abc.ext";
            log.Analyze(tooShortFileName);
            StringAssert.Contains("Filename too short: abc.ext", mockService.LastError);
        }
    }
}
