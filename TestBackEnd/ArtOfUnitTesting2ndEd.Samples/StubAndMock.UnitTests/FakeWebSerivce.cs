﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StubAndMock.UnitTests
{
    public class FakeWebSerivce : IWebService
    {
        public string LastError { get; set; }
        public void LogError(string message)
        {
            LastError = message;
        }
    }
}
