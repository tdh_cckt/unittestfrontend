﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StubAndMock.UnitTests
{
    public class FakeWebService2 : IWebService
    {
        public Exception ToThrow;
        public void LogError(string message)
        {
            if (ToThrow != null)
            {
                throw ToThrow;
            }
        }
    }
}
